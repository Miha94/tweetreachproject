<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            <div class="content">
                <div class="title m-b-md">
                    Tweet Reach
                </div>

                <input id="tweetUrl" type="text">

                <button id="requestButton" type="button">Request</button>
                <br>

               <h3 style="display: inline">Potential Reach Number: </h3>
                <p id="potentialNumber" style="display: inline; font-weight: bold;">

                </p>

                <p id="errorMessage" style="color:red"></p>

                <p id="loadingMessage" style="display: none; font-weight: bold;">
                    Loading...
               </p>
            </div>
        </div>

    <script src="https://code.jquery.com/jquery-3.2.1.min.js"
            integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
            crossorigin="anonymous"></script>

    <script>
        $(document).ready(function() {

            $('#requestButton').click(function(){

                $('#loadingMessage').show();

                var tweetUrl = $('#tweetUrl').val().replace(/\s/g, '');

                $('#errorMessage').text('');
                $('#potentialNumber').text('');

                $.ajax({
                    url: '/get_tweet_reach',
                    method: 'POST',
                    dataType: 'JSON',
                    data: {
                        tweet_url: tweetUrl
                    },
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },

                    success: function(response){
                        $('#loadingMessage').hide();

                        $('#potentialNumber').text(response.number);
                    },

                    error: function(error){

                        if(error.statusCode == 500)
                        {
                            $('#errorMessage').text('Error, please try again later!');

                            $('#loadingMessage').hide();

                            return;
                        }

                        var guzzle = JSON.parse(error.responseText);

                        $('#loadingMessage').hide();

                        if(guzzle.errorGuzzleMessage)
                        {
                            $('#errorMessage').text(guzzle.errorGuzzleMessage);
                        }
                        else {
                            $('#errorMessage').text(error.responseJSON.errors.tweet_url);
                        }
                    }
                })

            });

        })
    </script>
    </body>
</html>
