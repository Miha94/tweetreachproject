<?php

namespace App\Repository\Twitter;

use App\Repository\Repository;
use App\Tweet;

class TwitterRepository extends Repository {

    function model()
    {
        return $this->model = Tweet::class;
    }
}