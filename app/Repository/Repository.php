<?php

namespace App\Repository;

use App\Contracts\CriteriaInterface;
use App\Contracts\RepositoryInterface;
use App\Exceptions\RepositoryException;
use Illuminate\Container\Container;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use App\Criteria\Criteria;

abstract class Repository implements RepositoryInterface, CriteriaInterface {

    /**
     * @var Container.
     */
    private $app;

    /**
     * @var
     */
    protected $model;

    /**
     * @var Collection
     */
    protected $criteria;

    /**
     * @var bool
     */
    protected $skipCriteria = false;

    /**
     * @param Container $container
     * @param Collection $collection
     * @internal param App $app
     */
    public function __construct(Container $container, Collection $collection)
    {
        $this->app      = $container;
        $this->criteria = $collection;
        $this->resetScope();
        $this->makeModel();
    }

    /**
     * Specify Model class name.
     *
     * @return mixed
     */
    abstract function model();

    /**
     * @return Model
     * @throws \Exception
     */
    public function makeModel()
    {
        $model = $this->app->make($this->model());

        if (!$model instanceof Model)
            throw new RepositoryException("Class {$this->model()} must be an instance of Illuminate\\Database\\Eloquent\\Model");

        return $this->model = $model;
    }


    public function create(array $data)
    {
        return $this->model->create($data);
    }


    public function all($columns = ['*'])
    {
        $this->applyCriteria();
        return $this->model->get($columns);
    }


    public function update($id, array $data)
    {
        return $this->model->where('id', $id)->update($data);
    }


    public function delete($id)
    {
        return $this->model->destroy($id);
    }


    public function paginate($perPage = 10, $columns = ['*'])
    {
        $this->applyCriteria();
        return $this->model->paginate($perPage, $columns);
    }


    public function find($id, $columns = ['*'])
    {
        $this->applyCriteria();
        return $this->model->find($id, $columns);
    }


    public function findOrFail($id, $columns = ['*'])
    {
        $this->applyCriteria();
        return $this->model->findOrFail($id, $columns);
    }


    public function findByField($field, $value, $columns = ['*'])
    {
        $this->applyCriteria();
        return $this->model->where($field, $value)->first($columns);
    }


    public function pluck($column, $key = null)
    {
        $this->applyCriteria();
        return $this->model->pluck($column, $key);
    }


    public function sync($id, $relation, $attributes, $detaching = true)
    {
        return $this->find($id)->{$relation}()->sync($attributes, $detaching);
    }


    public function resetScope() {
        $this->skipCriteria(false);
        return $this;
    }


    public function skipCriteria($status = true){
        $this->skipCriteria = $status;
        return $this;
    }


    public function getCriteria() {
        return $this->criteria;
    }


    public function getByCriteria(Criteria $criteria) {
        $this->model = $criteria->apply($this->model, $this);
        return $this;
    }


    public function pushCriteria(Criteria $criteria) {
        $this->criteria->push($criteria);
        return $this;
    }


    public function  applyCriteria() {
        if($this->skipCriteria === true)
            return $this;

        foreach($this->getCriteria() as $criteria)
        {
            if($criteria instanceof Criteria)
                $this->model = $criteria->apply($this->model, $this);
        }

        return $this;
    }
}