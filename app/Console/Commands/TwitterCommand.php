<?php

namespace App\Console\Commands;

use App\Repository\Twitter\TwitterRepository;
use App\Services\TweetReachService;
use App\Tweet;
use Illuminate\Console\Command;

class TwitterCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tweetreach:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update all tweets in database';

    /**
     * @var TwitterRepository
     */
    private $twitterRepository;

    /**
     * Create a new command instance.
     *
     * @param TwitterRepository $repository
     */
    public function __construct(TwitterRepository $repository)
    {
        $this->twitterRepository = $repository;

        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        \Log::info('Tweets Update Started At: ' . date('Y-m-d H:i:s'));

        $tweets     = $this->twitterRepository->all();
        $service    = new TweetReachService();

        foreach ($tweets as $tweet)
        {
            $tweet_reach = $service->retweetsTweetReach($tweet->tweet_id);

            if(isset($retweetsTweetReach['errors'][0]['message']))
            {
                \Log::info("Failed to catch Tweet data id: {$tweet->tweet->id}");
                continue;
            }

            if(! $this->twitterRepository->update($tweet->id, ['tweet_reach' => $tweet_reach]))
                \Log::info("Failed to update Tweet id: {$tweet->tweet_id}");

        }

        \Log::info('Tweets Update Ended At: ' . date('Y-m-d H:i:s'));
    }
}
