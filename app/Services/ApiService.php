<?php

namespace App\Services;

use App\Contracts\ApiServiceInterface;

abstract class ApiService implements ApiServiceInterface {

    /**
     * Service name declaration.
     * @var string
     */
    protected $service;

    /**
     * Service base url
     * @var string
     */
    public $baseUrl;

    /**
     * Service url
     * @var string
     */
    public $url;

    /**
     * Url query for get methods
     * @var string
     */
    public $query;

    /**
     * Request type - method
     * @var string
     */
    public $method;

    /**
     * Headers
     * @var array
     */
    public $headers;

    public function setMethod($method)
    {
        $this->method = $method;

        return $this;
    }

    public function setUrl($url)
    {
        $this->url = $url;

        return  $this;
    }

    public function setQuery($query = '')
    {
        $this->query = $query;

        return $this;
    }

    public function sendRequest($headers = null)
    {
        $client = new \GuzzleHttp\Client();

        $url    = $this->baseUrl . $this->url . $this->query;

        $res    = $client->request( $this->method, $url, ['headers' => isset($headers) ? $headers : $this->headers]);

        return $res->getBody();
    }
}