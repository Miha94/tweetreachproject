<?php

namespace App\Services;

use App\Builders\ApiServiceBuilder;
use App\Builders\TwitterBuilder;
use App\Contracts\TweetReachInterface;
use GuzzleHttp\Exception\ClientException;

class TweetReachService implements TweetReachInterface {

    /**
     * Instance of TwitterService.
     * @var ApiService
     */
    protected $twitter;

    /**
     * Store All Users ids who share specific tweet.
     * @var array
     */
    protected $retweeters = [];

    /**
     * Store all potential users that saw tweet.
     *
     * @var array
     */
    protected $followers = [];

    /**
     * Total followers number from users who retweet status.
     *
     * @var integer
     */
    protected $followersNumber;


    public function __construct()
    {
        $this->twitter = ApiServiceBuilder::build($builder = new TwitterBuilder());
    }

    public function maxTweetReach($tweetID)
    {
        $tweet = $this->twitter->getTweetDataById($tweetID);

        if($tweet instanceof ClientException)
            return json_decode($tweet->getResponse()->getBody()->getContents(), true);

        $this->getAllRetweeters($tweetID);

    //Followers from tweet owner
        $this->getAllFollowers($tweet['user']['screen_name']);

    //Followers from mentioned users
        foreach ($tweet['entities']['user_mentions'] as $user)
        {
            $this->getAllFollowers($user['screen_name']);
        }
    //Followers from owners of the retweets
        foreach ($this->retweeters as $key => $value)
        {
            $this->getAllFollowers($this->twitter->getUserDataById($value)['screen_name']);
        }

        return count(array_unique($this->followers));
    }

    public function retweetsTweetReach($tweetID)
    {
        $retweets = $this->getAllRetweeters($tweetID);

        if($retweets instanceof ClientException)
            return json_decode($retweets->getResponse()->getBody()->getContents(), true);

    //Number of followers from owners of the retweets
        foreach ($this->retweeters as $key => $value)
        {
            $this->followersNumber += $this->twitter->getUserDataById($value)['followers_count'];
        }

        return $this->followersNumber;
    }

    public function getAllFollowers($screenName, $cursor = -1)
    {
        $user = $this->twitter->getUsersFollowers($screenName, $cursor);

        $this->followers += $user['ids'];

        if($user['next_cursor'] !== 0)
        {
            return $this->getAllFollowers($screenName, $user['next_cursor']);
        }

        return true;
    }

    public function getAllRetweeters($tweetID, $cursor = -1)
    {
        $retweeters = $this->twitter->getRetweetersIds($tweetID, $cursor);

        if($retweeters instanceof ClientException)
            return $retweeters;

        $this->retweeters += $retweeters['ids'];

        if($retweeters['next_cursor'] != 0)
        {
            return $this->getAllRetweeters($tweetID, $retweeters['next_cursor']);
        }

        return true;
    }
}