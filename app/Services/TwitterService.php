<?php

namespace App\Services;

use App\Traits\TwitterTrait;
use GuzzleHttp\Exception\ClientException;

class TwitterService extends ApiService {

    use TwitterTrait;

    /**
     * Receive all Tweet information.
     *
     * @param $id
     * @return mixed
     */
    public function getTweetDataById($id)
    {
        $this->setUrl('statuses/show.json')->setMethod('GET')->setQuery("?id={$id}");

        try{
            $response = $this->sendRequest($this->setHeaders());

        } catch (ClientException $e) {
            return $e;
        }

        return json_decode($response, true);
    }

    /**
     * Receive all User's information.
     *
     * @param $id
     * @return mixed
     */
    public function getUserDataById($id)
    {
        $this->setUrl('users/show.json')->setMethod('GET')->setQuery("?user_id={$id}");

        $response = $this->sendRequest($this->setHeaders());

        return json_decode($response, true);
    }

    /**
     * Receive list followers ids of specific user.
     *
     * @param $screenName
     * @param int $cursor
     * @param bool $stringifyIds
     * @return mixed
     * @internal param bool $skipStatus
     * @internal param bool $includeUserEntities
     */
    public function getUsersFollowers($screenName, $cursor = -1, $stringifyIds = true)
    {
        $this->setUrl('followers/ids.json')->setMethod('GET')->setQuery("?cursor={$cursor}&screen_name={$screenName}&stringify_ids={$stringifyIds}");

        $response = $this->sendRequest($this->setHeaders());

        return json_decode($response, true);
    }

    /**
     * Receive users ids that retweeted specific tweet.
     *
     * @param $id
     * @return mixed
     */
    public function getRetweetersIds($id)
    {
        $this->setUrl('statuses/retweeters/ids.json')->setMethod('GET')->setQuery("?id={$id}");

        try{
            $response = $this->sendRequest($this->setHeaders());

        } catch (ClientException $e) {
            return $e;
        }

        return json_decode($response, true);
    }

}