<?php

namespace App\Traits;

trait TwitterTrait {

    /**
     * OAuth headers setup
     * @return array
     */
    protected function setHeaders()
    {
        $authorizationArray = $this->headers + [
            'oauth_nonce'     => time(),
            'oauth_timestamp' => time(),
        ];

        $authorizationArray['oauth_signature'] = $this->generateOauthSignature($authorizationArray);

        return [
            'Authorization' => $this->createAuthorizationHeader($authorizationArray),
            'Expect' => ''
        ];
    }

    /**
     * Build authorization header
     * @param array $params
     * @return string
     */
    private function createAuthorizationHeader(array $params)
    {
        foreach($params as $key => $value)
        {
            if (in_array($key, [
                'oauth_consumer_key',
                'oauth_nonce',
                'oauth_signature',
                'oauth_signature_method',
                'oauth_timestamp',
                'oauth_token',
                'oauth_version'
            ]))
            {
                $values[] = "$key=\"" . rawurlencode($value) . "\"";
            }
        }

        return 'OAuth ' . implode(', ', $values);
    }

    /**
     * Generate oauth signature
     * @param $params
     * @return string
     */
    protected function generateOauthSignature($params)
    {
        parse_str(ltrim($this->query, '?'), $query);

        $params = array_merge($params, $query);

        ksort($params);

        foreach($params as $key => $value)
        {
            $rawUrlArray[] = rawurlencode($key) . '=' . rawurlencode($value);
        }

        $baseInfo       = $this->method . "&" . rawurlencode($this->baseUrl.$this->url) . '&' . rawurlencode(implode('&', $rawUrlArray));
        $compositeKey   = rawurlencode(env('TWITTER_CONSUMER_SECRET')) . '&' . rawurlencode(env('TWITTER_ACCESS_TOKEN_SECRET'));

        return base64_encode(hash_hmac('sha1', $baseInfo, $compositeKey, true));
    }

}