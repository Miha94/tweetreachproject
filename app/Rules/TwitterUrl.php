<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class TwitterUrl implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $urlArray = explode("/", $value);

        if(count($urlArray) != 6)
            return false;

        if( $urlArray[2] != 'twitter.com' || $urlArray[3] == '' || $urlArray[4] != 'status')
            return false;

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return ':attribute is not a valid twitter url!';
    }
}
