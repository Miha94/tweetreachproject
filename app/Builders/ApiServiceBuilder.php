<?php

namespace App\Builders;

use App\Contracts\ApiBuilderInterface;
use App\Services\ApiService;

class ApiServiceBuilder {

    /**
     * Builder class for building API Services
     *
     * @param ApiBuilderInterface $builder
     * @return ApiService
     */
    public static function build(ApiBuilderInterface $builder)
    {
        $builder->createApiService();
        $builder->setBaseUrl();
        $builder->setHeaders();

        return $builder->getApiService();
    }
}