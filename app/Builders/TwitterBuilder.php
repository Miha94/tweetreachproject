<?php

namespace App\Builders;

use App\Contracts\ApiBuilderInterface;
use App\Services\TwitterService;
use App\Traits\TwitterTrait;

class TwitterBuilder implements ApiBuilderInterface {

    use TwitterTrait;

    /**
     * @var \App\Services\TwitterService
     */
    private $twitter;

    /**
     * Create new instance of the TwitterService.
     */
    public function createApiService()
    {
        $this->twitter = new TwitterService();
    }

    /**
     * Return completely built TwitterService instance.
     *
     * @return TwitterService
     */
    public function getApiService()
    {
        return $this->twitter;
    }

    /**
     * Set Base URL from the env file.
     */
    public function setBaseUrl()
    {
        $this->twitter->baseUrl = env('TWITTER_BASE_URL');
    }

    /**
     * Set Twitter Headers.
     */
    public function setHeaders()
    {
        $this->twitter->headers = [
                'oauth_consumer_key'     => env('TWITTER_CONSUMER_KEY'),
                'oauth_token'            => env('TWITTER_ACCESS_TOKEN'),
                'oauth_version'          => env('TWITTER_OAUTH_VERSION'),
                'oauth_signature_method' => 'HMAC-SHA1',
        ];
    }
}