<?php

namespace App\Contracts;

interface ApiServiceInterface {

    /**
     * Set method - request type
     * @param $method
     * @return $this
     */
    public function setMethod($method);
    /**
     * Set url
     * @param $url
     * @return $this
     */
    public function setUrl($url);

    /**
     * Set query for get methods
     * @param string $query
     * @return $this
     */
    public function setQuery($query = '');

    /**
     * Send request via \GuzzleHttp\Client
     *
     * @return \Psr\Http\Message\StreamInterface
     */
    public function sendRequest();
}