<?php

namespace App\Contracts;

interface TweetReachInterface {

    /**
     * Calculate maximum tweet reach.
     *
     * @param $tweetID
     * @return int
     */
    public function maxTweetReach($tweetID);

    /**
     * Calculate total followers from users who retweet specific tweet;
     *
     * @param $tweetID
     * @return mixed
     */
    public function retweetsTweetReach($tweetID);

    /**
     * Get all followers from specific user.
     *
     * @param $screenName
     * @param int $cursor
     * @return bool
     */
    public function getAllFollowers($screenName, $cursor = -1);

    /**
     * Get all users ids who are retweeted specific tweet.
     *
     * @param $tweetID
     * @param int $cursor
     * @return bool
     */
    public function getAllRetweeters($tweetID, $cursor = -1);
}