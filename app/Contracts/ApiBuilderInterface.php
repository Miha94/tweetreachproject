<?php

namespace App\Contracts;

interface ApiBuilderInterface {

    public function createApiService();

    public function getApiService();

    public function setBaseUrl();

    public function setHeaders();
}