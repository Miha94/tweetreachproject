<?php

namespace App\Contracts;

interface RepositoryInterface {

    /**
     * Create model.
     *
     * @param array $data
     * @return mixed
     */
    public function create(array $data);

    /**
     * Get all of the models data from the database.
     *
     * @param array $columns
     * @return mixed
     */
    public function all($columns = ['*']);

    /**
     * Update model data.
     *
     * @param $id
     * @param array $data
     * @return mixed
     */
    public function update($id, array $data);

    /**
     * Delete model.
     *
     * @param $id
     * @return mixed
     */
    public function delete($id);

    /**
     * Paginate model data.
     *
     * @param int $perPage
     * @param array $columns
     * @return mixed
     */
    public function paginate($perPage = 10, $columns = ['*']);

    /**
     * Find model data by id.
     *
     * @param $id
     * @param array $columns
     * @return mixed
     */
    public function find($id, $columns = ['*']);

    /**
     * Find model.
     *
     * @param $id
     * @param array $columns
     * @return mixed
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException;
     */
    public function findOrFail($id, $columns = ['*']);

    /**
     * Find model data by field and value.
     *
     * @param       $field
     * @param       $value
     * @param array $columns
     * @return mixed
     */
    public function findByField($field, $value, $columns = ['*']);

    /**
     * Retrieve data array for populate field select.
     *
     * @param string $column
     * @param string|null $key
     * @return \Illuminate\Support\Collection|array
     */
    public function pluck($column, $key = null);

    /**
     * Sync relations.
     *
     * @param $id
     * @param $relation
     * @param $attributes
     * @param bool $detaching
     * @return mixed
     */
    public function sync($id, $relation, $attributes, $detaching = true);

//    ...
}