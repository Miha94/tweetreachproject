<?php

namespace App\Criteria;

use App\Contracts\RepositoryInterface;
use Carbon\Carbon;

class OlderThanMonthAndMoreThanThousandTweetReach extends Criteria {

    /**
     * @param $model
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $query  = $model->whereDate('created_at', '<', Carbon::today()->subMonth())
                        ->where('tweet_reach', '>', 1000);

        return $query;
    }
}