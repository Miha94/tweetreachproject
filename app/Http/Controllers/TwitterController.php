<?php

namespace App\Http\Controllers;

use App\Http\Requests\GetTweetId;
use App\Repository\Twitter\TwitterRepository;
use App\Service\Tweet\CreateTweetService;
use App\Services\TweetReachService;
use App\Tweet;
use Illuminate\Http\Request;

class TwitterController extends Controller
{

    /**
     * @var TwitterRepository
     */
    private $twitterRepository;

    public function __construct(TwitterRepository $repository)
    {
        $this->twitterRepository = $repository;
    }

    public function getTweetReach(GetTweetId $request)
    {
        $urlArray = explode("/", $request->get('tweet_url'));
        $tweetID  = end($urlArray);

        if($tweet = $this->twitterRepository->findByField('tweet_id', $tweetID))
            return response()->json(['number' => $tweet->tweet_reach], 200);

        $retweetsTweetReach = (new TweetReachService())->retweetsTweetReach($tweetID);

        if(isset($retweetsTweetReach['errors'][0]['message']))
            return response()->json(['errorGuzzleMessage' => $retweetsTweetReach['errors'][0]['message']], 404);

        $this->twitterRepository->create([
            'tweet_id'    => $tweetID,
            'tweet_reach' => $retweetsTweetReach
        ]);

        return response()->json(['number' => $retweetsTweetReach], 200);
    }
}
