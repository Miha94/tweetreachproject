<?php

namespace Tests\Feature;

use App\Repository\Twitter\TwitterRepository;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class TweetReachTest extends TestCase
{

    use DatabaseTransactions, DatabaseMigrations;

    /**
     * Test Tweet Reach.
     *
     * @return void
     */
    public function testTweetReach()
    {
        $response = $this->json('POST', route('tweet_reach'),
            [
                'tweet_url' => 'https://twitter.com/Jokanovic/status/911638919707598848'
            ]);

        $response->assertSuccessful();

        $this->assertArrayHasKey('number', $response->getOriginalContent());
    }

    /**
     * Test Invalid Tweet Reach.
     *
     * @return void
     */
    public function testInvalidTweetReach()
    {
        $response = $this->json('POST', route('tweet_reach'),
            [
                'tweet_url' => 'https://invalid/Tweeterurl'
            ]);

        $response->assertStatus(422);
    }
}
