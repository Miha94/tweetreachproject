<?php

namespace Tests\Unit;

use App\Repository\Twitter\TwitterRepository;
use Illuminate\Container\Container;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App;
use Tests\TestCase;

class TwitterRepositoryTest extends TestCase
{
    use DatabaseTransactions, DatabaseMigrations;

    private $twitterRepo;

    private $columns = [
        'tweet_reach',
        'created_at'
    ];

    public function setUp()
    {
        parent::setUp();

        $this->twitterRepo = new TwitterRepository($container = new Container, $collection = new Collection);
    }

    /**
     * @return mixed
     */
    public function testPerformsCreate()
    {
        $data = [
            'tweet_id'      => rand(10000, 99999),
            'tweet_reach'   => rand(9, 9999),
        ];

        $result = $this->twitterRepo->create($data);

        $this->assertEquals($data['tweet_id'], $result->tweet_id);
        $this->assertEquals($data['tweet_reach'], $result->tweet_reach);

        return $result;
    }

    /**
     * Test update of previously created model.
     *
     * @return void
     */
    public function testPerformsUpdate()
    {
        $data = [
            'tweet_id'      => rand(10000, 99999),
            'tweet_reach'   => rand(9, 9999),
        ];

        $create = $this->testPerformsCreate();

        $result = $this->twitterRepo->update($create->id, $data);

        $this->assertTrue((boolean) $result);
    }

    /**
     * Test delete previously deleted model.
     *
     * @return void
     */
    public function testPerformsDelete()
    {
        $create = $this->testPerformsCreate();

        $result = $this->twitterRepo->delete($create->id);

        $this->assertTrue((boolean) $result);
    }

    /**
     * Test repository find method.
     *
     * @return void
     */
    public function testPerformsFind()
    {
        $create = $this->testPerformsCreate();

        $result = $this->twitterRepo->find($create->id);

        $this->assertEquals($create->id, $result->id);
    }

    /**
     * Test repository find method with given fields.
     *
     * @return void
     */
    public function testPerformsFindWithGivenColumns()
    {
        $create = $this->testPerformsCreate();

        $result = $this->twitterRepo->find($create->id, $this->columns);

        $this->checkIfInArray($result);
    }

    /**
     * Test repository findOrFail method.
     *
     * @return void
     */
    public function testPerformsFindOrFail()
    {
        $create = $this->testPerformsCreate();

        $result = $this->twitterRepo->findOrFail($create->id);

        $this->assertEquals($create->id, $result->id);
    }

    /**
     * Test repository findOrFail method with given fields.
     *
     * @return void
     */
    public function testPerformsFindOrFailWithGivenColumns()
    {
        $create = $this->testPerformsCreate();

        $result = $this->twitterRepo->findOrFail($create->id, $this->columns);

        $this->checkIfInArray($result);
    }

    /**
     * Test repository findByField method.
     *
     * @return void
     */
    public function testPerformsFindByField()
    {
        $create = $this->testPerformsCreate();

        $result = $this->twitterRepo->findByField('tweet_id', $create->tweet_id);

        $this->assertEquals($create->id, $result->id);
    }

    /**
     * Test repository findByField method with given fields.
     *
     * @return void
     */
    public function testPerformsFindByFieldWithGivenColumns()
    {
        $create = $this->testPerformsCreate();

        $result = $this->twitterRepo->findByField('tweet_id', $create->tweet_id, $this->columns);

        $this->checkIfInArray($result);
    }

    /**
     * Test repository pluck method.
     *
     * @return void
     */
    public function testPerformsPluck()
    {
        $column = 'tweet_id';

        factory(App\Tweet::class, 5)->create();

        $result = $this->twitterRepo->pluck($column);

        foreach ($result as $res)
        {
            $find = $this->twitterRepo->findByField($column, $res);

            $this->assertEquals($find->$column, $res);
        }
    }

    /**
     * Test repository pluck method with provided key.
     *
     * @return void
     */
    public function testPerformsPluckWithKey()
    {
        $column     = 'tweet_id';
        $pluckKey   = 'tweet_reach';

        factory(App\Tweet::class, 5)->create();

        $result = $this->twitterRepo->pluck($column, $pluckKey);

        foreach ($result as $key => $value)
        {
            $find = $this->twitterRepo->findByField($pluckKey, $key);

            $this->assertEquals($find->$column, $value);
            $this->assertEquals($find->$pluckKey, $key);
        }
    }

    /**
     * Test repository all method.
     *
     * @return void
     */
    public function testPerformsAll()
    {
        $number = 20;

        factory(App\Tweet::class, $number)->create();

        $result = $this->twitterRepo->all();

        $this->assertEquals($number, $result->count());
    }

    /**
     * Test repository all method with given columns.
     *
     * @return void
     */
    public function testPerformsAllWithGivenColumns()
    {
        $number = 20;

        factory(App\Tweet::class, $number)->create();

        $result = $this->twitterRepo->all($this->columns);

        foreach ($result as $res)
        {
            $this->checkIfInArray($res);
        }
    }

    /**
     * Test repository paginate method.
     *
     * @return void
     */
    public function testPerformsPaginate()
    {
        $perPage = 15;

        factory(App\Tweet::class, 100)->create();

        $result = $this->twitterRepo->paginate($perPage);

        $this->assertObjectHasAttribute('currentPage', $result);
        $this->assertObjectHasAttribute('perPage', $result);
        $this->assertObjectHasAttribute('total', $result);
        $this->assertObjectHasAttribute('lastPage', $result);
        $this->assertEquals($perPage, $result->count());
    }

    /**
     * Test repository paginate method with given columns.
     *
     * @return void
     */
    public function testPerformsPaginateWithGivenColumns()
    {
        $perPage = 50;

        factory(App\Tweet::class, 100)->create();

        $result = $this->twitterRepo->paginate($perPage, $this->columns);

        $this->assertObjectHasAttribute('currentPage', $result);
        $this->assertObjectHasAttribute('perPage', $result);
        $this->assertObjectHasAttribute('total', $result);
        $this->assertObjectHasAttribute('lastPage', $result);
        $this->assertEquals($perPage, $result->count());

        foreach($result as $res)
        {
            $this->checkIfInArray($res);
        }
    }

    /**
     * Check if exists in columns array.
     *
     * @param $result
     */
    private function checkIfInArray($result)
    {
        foreach($result->getAttributes() as $key => $value)
        {
            $this->assertContains($key, $this->columns);
        }
    }
}
