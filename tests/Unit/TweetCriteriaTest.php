<?php

namespace Tests\Unit;

use App\Criteria\OlderThanMonthAndMoreThanThousandTweetReach;
use App\Repository\Twitter\TwitterRepository;
use App\Tweet;
use Carbon\Carbon;
use Illuminate\Container\Container;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class TweetCriteriaTest extends TestCase
{
    use DatabaseMigrations, DatabaseMigrations;

    private $twitterRepo;

    public function setUp()
    {
        parent::setUp();

        $this->twitterRepo = new TwitterRepository($container = new Container, $collection = new Collection);
    }

    /**
     * Testing given criteria.
     *
     * @return void
     */
    public function testPerformsCriteria()
    {
        factory(Tweet::class, 20)->create();

        $result = $this->twitterRepo->pushCriteria(new OlderThanMonthAndMoreThanThousandTweetReach())->all();

        foreach ($result as $res)
        {
            $this->assertGreaterThan(1000, $res->tweet_reach);
            $this->assertLessThan(Carbon::today()->subMonth()->format('Y-m-d'), $res->created_at->format('Y-m-d'));
        }
    }
}
