<?php

use Faker\Generator as Faker;

$factory->define(\App\Tweet::class, function (Faker $faker) {
    return [
        'tweet_id'      => $faker->randomNumber(),
        'tweet_reach'   => $faker->randomNumber(),
        'created_at'    => $faker->dateTimeBetween('-6 months', 'now')
    ];
});
